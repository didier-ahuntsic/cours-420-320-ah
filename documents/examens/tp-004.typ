#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points
#import "../common.typ": answer-line, code-block-answer

#let points-state = state("points", ());

#let extra-header = [
    Cet examen contient #total-points(points-state, <end>)

    Nom: #overline(offset: 0em, [`                                        `]) \
    Numéro d'étudiant: #overline(offset: 0em, [`                            `])
]

#let tic-tac-toe = link("https://fr.wikipedia.org/wiki/Tic-tac-toe")[tic-tac-toe]

#let fixtures = link("https://docs.pytest.org/en/6.2.x/fixture.html")[_fixtures_]


#show: basic-footer

#show: doc => basic-header(
    "travail Pratique IV | Test-driven development", 
    doc
    )

#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


= Instruction

- Ce quatrième travail pratique doit être remis pour le 23 décembre 2023 à 23h59.
- Le  travail doit être fait individuellement.
- Le travail doit être remis sous la forme d'un repository github  par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


= Énoncé

#set heading(numbering: numbering-function, level: 1)



== Calculatrice textuelle

En utilisant la méthode du test-driven development, vous devez implémenter
requis suivants.

=== #points(none, 5, points-state) Initialisation

Créez une fonction add qui prend un `str` et renvoie un `str`:

1. La méthode peut prendre 0, 1 ou 2 nombres séparés par une virgule, et renvoie leur somme.
2. Une chaîne vide renverra "0".

*Exemples*
#table(
  columns: (auto, auto),
  inset: 10pt,
  align: horizon,
  [*input*], [*output*],
  [`""`], [`"0"`],
  [`"3"`], [`"3"`],
  [`"17,3"`], [`"20"`],
)

=== #points(none, 5, points-state) Plusieurs nombres
Permettre à la fonction de gérer un nombre arbitraire de nombres à l'entrée.

*Exemples*

#table(
  columns: (auto, auto),
  inset: 10pt,
  align: horizon,
  [*input*], [*output*],
  [`"17,3,5,2,3"`], [`"30"`],
)

=== #points(none, 5, points-state) Nouvelle ligne comme sépatateur

Permettre à la fonction de gérer les nouvelles lignes comme séparateurs.

*Exemples*
#table(
  columns: (auto, auto),
  inset: 10pt,
  align: horizon,
  [*input*], [*output*],
  [`"17\n3"`], [`"20"`],
)

=== #points(none, 5, points-state) Nombre manquant à la dernière position
Permettre que l'entrée finisse par un séparateur.

*Exemples*
#table(
  columns: (auto, auto),
  inset: 10pt,
  align: horizon,
  [*input*], [*output*],
  [`"17\n3,"`], [`"20"`],
  [`","`], [`"0"`],
)

== Compter le nombre de ligne dans un document

En utilisant la méthode du test-driven development, écrire une fonction 
qui prend le chemin d'un fichier en paramêtre et retourne le nombre de 
lignes dans le document.

*Évaluation*

1. #points(none, 5, points-state) Implémentation de la fonctionnalitée.

2. #points(none, 5, points-state) Suite de tests provenant de la méthodologie test-driven development.

3.  #points(none, 10, points-state) Utilisation de #fixtures dans les tests.

== Tic-Tac-Toe

En utilisant la méthode du test-driven development, écrire une classe
qui permet de jouer au #tic-tac-toe.  

La classe doit permettre de:

1. Au joueur X de jouer
2. Au joueur O de jouer
3. De savoir si la partie est terminée. 
4. De savoir qui à gagné la partie.


*Évaluation*

1. #points(none, 10, points-state) Implémentation des 4 fonctionnalitées.

2. #points(none, 30, points-state) Suite de tests provenant de la méthodologie test-driven development.

3.  #points(none, 10, points-state) Isolation des données de tests dans des #fixtures.

<end>