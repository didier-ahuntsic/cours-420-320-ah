#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique III | Lists, Objets et Itérations", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


= Instructions

- Ce troisième devoir doit être remis pour le 15 novembre 2023 à 23h59.
- Le  travail doit être fait individuellement.
- Le travail doit être remis sous la forme d'un projet 
  compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README avec votre nom (5% sera enlevé sinon).
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


== Quoi remettre

Le dossier compréssé à remettre doit comprendre un README où vous écriverez
votre nom.  Tout le code doit se trouver dans un sous dossier `src`.  La 
page web principale doit se nommer `index.html`.  Ici est un exemple du résultat.

```
README.md
src/
├─ index.html
├─ script.js

```


== Bonne chance
Si vous avez des question, n'hésite pas à contacter votre enseignant au didier.amyot\@collegeahuntsic.qc.ca ou par mio.


= Énoncé

#rect(width: 100%, stroke: black)[
=== Truc

1. Lire et comprendre la question du point de vue de l'utilisateur.
2. Mettre la page web dans l'état attendu juste avant l'action.
3. Ouvrir l'inspecteur de chrome et essayer de faire l'exercice dans la console JavaScript.
4. Extraire la logique dans une fonction.
5. Mettre la fonction dans le fichier JavaScript (`script.js`).
6. Linker la fonction et le html avec un event approprié (`onclick`)
]


=== Exemples

Une implémentation du devoir est accessible au https://didier-ahuntsic.gitlab.io/cours-420-298-ah/examples/tp3.html.
Vous pouvez utiliser cette implémentation pour vous assurer de bien comprendre les questions.




== Lists, Objets et Itérations

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/tp3.html et modifier le code source.

=== Données
Certains numéros de cette question requièrent d'avoir accès aux données ci-bas.
==== Villes 
La liste à utiliser pour les villes est accessible à l'URL suivant.
https://gitlab.com/didier-ahuntsic/cours-420-298-ah/-/blob/main/cours/bases/villes.json



==== Personnes 
La liste à utiliser pour les personnes est accessible à l'URL suivant.
https://gitlab.com/didier-ahuntsic/cours-420-298-ah/-/blob/main/cours/bases/personnes.json


#set heading(numbering: numbering-function, level: 1)


=== Linker un script extérieur

#points(none, 1, points-state) 

Ajourter une balise `script` tout en bas du body.  Le script associé à la balise doit assigner le texte
"Travail Pratique III" à l'élément avec l'id `main-title`.


=== Sum de tous les nombres de 1 à $n$

#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur `#boutto