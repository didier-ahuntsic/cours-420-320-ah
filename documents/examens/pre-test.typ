#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Pré Examen", doc)

#let home_os = [
    Quel système d'exploitation utilisez-vous pour développer habituellement?
    #answer-choices(("Linux", "macOS", "Microsoft Windows", "ChromeOS"))
]

#let why_learning = [
    Pourquoi voulez vous apprendre à programmer?
    #answer-line()
    #answer-line()
]

#let when-programming-solve-issues = [
    Avez-vous déjà été dans une situation où vous auriez aimé être capable
    de programmer pour régler un problème concret?  Si oui, décrivez
    la situation.
    #answer-line()
    #answer-line()
]


#let github = [
    Avez-vous déjà programmer? Si oui avec quel language?
    #answer-line()
]

#let ide = [
    Avez-vous déjà utilisé un environnement de développement (IDE)? Si oui, lequel?
    #answer-line()
]


#let db = [
    Avez-vous déjà utilisé une base de données? Si oui, laquelle?
    #answer-line()
]

#let webserver = [
    Avez-vous déjà fait un site web?  Si oui, avec quoi?
    #answer-line()
]


#let what_is_ls = [
    Êtes-vous confortable dans un terminal?
    #answer-line()
]


#let average_function = [
    Pouvez-vous écrire une fonction qui calcule la moyenne d'une liste de nombres dans ce language?
    #code-block-answer
]

#let simple_arythimethic_question = [
    Que vaut $2 + 3 times 5$ ? 
    #answer-line()
]

#let ms-excel = [
    Êtes-vous un·e roi/reine· d'Excel? 
    #answer-line()
]

#let english = [
    Are you able to maintain a conversation in English?  Do you speak another language than English or French?
    #answer-line()
]

#enum(
    tight: false,
    enum.item(home_os),
    enum.item(why_learning),
    enum.item(when-programming-solve-issues),
    enum.item(github),
    enum.item(ide),
    enum.item(db),
    enum.item(webserver),
    enum.item(what_is_ls),
    enum.item(simple_arythimethic_question),
    enum.item(ms-excel),
    enum.item(english),
    

)