#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique I | Git", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}

= Instruction

- Ce deuxième devoir doit être remis pour le 10 novembre 2023 à 13h00.
- Le  travail doit être fait individuellement.
- Le travail doit être remis sous la forme d'un repository github  par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


- https://training.github.com/downloads/fr/github-git-cheat-sheet/

= Énoncé

#set heading(numbering: numbering-function, level: 1)

== Créer un repository sur github
#points(none, 2, points-state) 

Sur votre github, créer une repository publique.

== Cloner le repository
#points(none, 0, points-state) 

Cloner le repository localement.


== Ajouter une fichier 
#points(none, 2, points-state) 

Ajouter un fichier `main.py` avec le code ci-bas.  
Committer et pusher le fichier.

```python
import click

@click.command("hello")
def hello():
    click.echo("Hello, World!")

if __name__ == "__main__":
    hello()
```
https://gist.github.com/didiercrunch/97ebc4be510d8f4d0340c68753c4c5d9


== Changer le fichier 
#points(none, 2, points-state) 

Changer le fichier `main.py` avec le code ci-bas.  
Committer et pusher le changement.

```python
import click

__version__ = "0.1.0"

@click.command("version")
def version():
    click.echo(__version__)

@click.command("hello")
def hello():
    click.echo("Hello, World!")

if __name__ == "__main__":
    hello()
```
https://gist.github.com/didiercrunch/e6d446b5381613cffebab3a28fa11cc5

== Nouvelle branch
#points(none, 2, points-state) 

Créer une nouvelle branch nommé `bonjour` et pusher la branche.

== Modifier fichier sur la branche `bonjour`
#points(none, 2, points-state) 

Sur la branche `bonjour`, changer le fichier `main.py` avec le code ci-bas.  Committer et pusher le changement.

```python
import click

__version__ = "0.1.1-SNAPSHOT"

@click.command("bonjour")
def bonjour():
    click.echo("bonjour monde")

@click.command("version")
def version():
    click.echo(__version__)

@click.command("hello")
def hello():
    click.echo("Hello, World!")

if __name__ == "__main__":
    hello()
```
https://gist.github.com/didiercrunch/e60114951e524f97fffbcb3c8c877efe

== Retourner sur la branche `main`.
#points(none, 0, points-state) 

Retourner sur la branche `main`.

== Modifier fichier sur la branche `main`
#points(none, 2, points-state) 

Sur la branche `main`, changer le fichier `main.py` avec le code ci-bas.  Committer et pusher le changement.

```python
import click

__version__ = "0.1.1"

@click.command("guten-tag")
def bonjour():
    click.echo("guten tag")

@click.command("version")
def version():
    click.echo(__version__)

@click.command("hello")
def hello():
    click.echo("Hello, World!")

if __name__ == "__main__":
    hello()
```
https://gist.github.com/didiercrunch/75c3f6bc0254de2b9ab4a01eb945b6a9

== Merger les branches
#points(none, 4, points-state) 

Merger la branch `bonjour` dans la branch `main` et pusher.


<end>