#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points
#import "../common.typ": answer-line, code-block-answer

#let points-state = state("points", ());

#let extra-header = [
    Cet examen contient #total-points(points-state, <end>)

    Nom: #overline(offset: 0em, [`                                        `]) \
    Numéro d'étudiant: #overline(offset: 0em, [`                            `])
]

#show: basic-footer

#show: doc => basic-header(
    "travail Pratique I | Git", 
    extra-header-line: extra-header,
    doc
    )

#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


#set heading(numbering: numbering-function, level: 1)



=== #points(none, 10, points-state) Quelle est la différence entre l'agilité et le "scrum"?

#answer-line()
#answer-line()

=== #points(none, 10, points-state) Quelle document explique la technique "scrum" (et que vous avez lu avec enthousiasm?)

#answer-line()
#answer-line()

=== #points(none, 10, points-state) Quelle est la durée normal d'un sprint?

#answer-line()
#answer-line()


=== #points(none, 10, points-state) Quelle est le nom des 3 rôles dans une équipe scrum?

#answer-line()
#answer-line()
#answer-line()

=== #points(none, 10, points-state) Quelle est la différence entre les tickets du "Product Backlog" et les tickets faisant partie d'un sprint?

#answer-line()
#answer-line()
#answer-line()

=== #points(none, 10, points-state) Quelle est la différence entre les tickets du "Product Backlog" et les tickets faisant partie d'un sprint?
#answer-line()

=== #points(none, 10, points-state) Qui s'occupe du "Product Backlog"?
#answer-line()

=== #points(none, 10, points-state) À quoi sert le "daily standup"?
#answer-line()
#answer-line()
#answer-line()

=== #points(none, 10, points-state) À quoi sert le "sprint review"?
#answer-line()
#answer-line()
#answer-line()


=== #points(none, 10, points-state) À quoi sert le "sprint retrospective"?
#answer-line()
#answer-line()
#answer-line()



<end>