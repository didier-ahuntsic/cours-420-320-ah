#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique I | Git", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}

= Instruction

- Ce deuxième devoir doit être remis pour le 22 novembre 2023 à 13h00.
- Les oraux auront lieux au début du cours du 22 novembre 2023.
- Le  travail doit être fait en équipe de 1 à 3.
- Le travail doit être remis sous la forme d'un repository github  par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.

= Énoncé

Vous travaillez sur un système de gestion de livres dans une bibliothèque.  Le système 
existe déjà et permet aux usagés de chercher des livres dans la bibliothèque.  Les
bibliothèquaires veullent être capable d'aujouter elles-même des livres au système.

L'architecture du système ressemble à au schema ci-bas.  Les changements peuvent affecter
le frontend ou le backend.

#figure(
  image("architecture.png", width: 80%),
  caption: [
    Architecture du système de la bibliothèque.
  ],
)

Normalement, l'ajout d'un livre commence par l'entrée de son code ISBN, qui servira 
d'identifiant unique pour trouver le livre. Le système doit ensuite vérifier que l'ISBN 
entré est bien valide. Le système vérifie ensuite si l'ISBN existe déjà dans le système. 
Si c'est le cas, il va refuser l'ajout, vu que le livre existe déjà dans le système. Si 
le livre n'existe pas déjà, alors le système va permettre à l'utilisateur d'ajouter les 
détails sur le livre (auteur, année de publication, etc). 




La difficulté est que la bibliothèque
gère beaucoup de livres usagés, et que les livres publiés avant 1972 n'ont pas d'ISBN. Il doit 
donc aussi être possible d'ajouter un livre qui ne possède pas d'ISBN. Dans ce cas, il est 
obligatoire d'entrer un titre, qui servira d'identifiant unique pour trouver le livre.



Votre travail consiste à écrire une proposition pour changer le système.  Votre proposition
doit être revue par vos coéquipiers avant de commencer l'implémentation; vous devez donc convaincre
vos coéquipiers du bienfondé de votre idée.


1. #points(none, 10, points-state) Écrire un document markdown, dans un repository github, avec une proposition de solution.

1. #points(none, 10, points-state) Le document markdown doit contenir au moins un diagram séquence et un diagram use-case.

1. #points(none, 10, points-state) Présenter votre proposition au reste de la classe.



<end>