#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


Tous les exercices suivant doivent être fait en utilisant la méthode TDD.

=== FizzBuzz

Écrire une fonction qui, pour les nombres entre 1 et 100 :
si le nombre est divisible par 3 : on écrit Fizz
si le nombre est divisible par 5 : on écrit Buzz
si le nombre est divisible par 3 et par 5 : on écrit Fizzbuzz
sinon : on écrit le nombre


=== Calculatrice
Nous allons écrire une fonction `Add` qui prendra un texte (`str` en python) et retournera un nombre suivant les requis plus bas.


==== Requis

1. La fonction devra prendre un text contenant 0, 1 ou 2 nombre séparé par une virgule et retourner leurs somme.  Par exemple, si l'entré est `"1,4"` la fonction retournera `5`, pour l'entré `"7"` la fonction retournera `7` et pour l'entrée vide (`""`) la fonction retournera zero (`0`).


2. Permettre à la fonction `Add` de prendre un nombre quelconque de nombre dans l'entrée. Par exemple,si l'entré est `"1,4,3,2"` la fonction retournera `10`.

3. Permettre à la fonction de prendre une entrée où les séparateurs sont des `\n` au lieu de `,`.

4. Ajouter une validation qui empêche l'ajout d'un séparateur à la fin.  Si la fonction recoit `1,2,` en entrée, elle doit lancer une erreur.

5. Ajouter une validation qui empêche l'entrée de nombres strictement négatif.  Si la fonction recoit `21,-22,190` en entrée, elle doit lancer une erreur.