# Ajout de livres dans la bibliothèques


## Idée principale

Écrire la majorité de la logique dans le front-end de manière optimiste.  Le front-end
demandera de l'information au backend si nécessaire mais l'opération d'écriture se 
fera dans une seule requête.  Si l'opération d'écriture échoue, l'utilisateur devra
reprendre du début.



## Séparation du travail

### Front-end
Le front-end devra s'assurer de choisir quel workflow l'utilisateur doit prendre.  Le
front-end utilisera le back-end comme une base de donnée.

![front-end](./front-end.png)

### Back-end
Le back-end devra offrir une interface aussi REST que possible au front-end.


## Diagramme en séquence

```mermaid
sequenceDiagram
    User->>Frontend: Je veux créer un lire
    User->>Frontend: ISBN (peut-être vide)
    User->>Frontend: Titre
    User->>Frontend: Année de publication
    User->>Frontend: Créer livre avec information présente
    Frontend->>Backend:Est-ce que le livre existe déjà?
    Backend-->>Frontend:Livre existe déjà oui/non
    alt le livre n'existe pas
        Frontend->>Backend: Créer livre
        Backend-->>Frontend:validation de la création de livre
        Frontend-->>User:Message de success
    else le livre existe
        Frontend-->>User:Montrer message d'erreur
    end

```



[^1]: https://en.wikipedia.org/wiki/ISBN