(require '[cheshire.core :as json])

(def jupyter-template {:metadata 
{:celltoolbar "Slideshow", 
:kernelspec {:display_name "Python 3", :language "python", :name "python3"}, :language_info {:codemirror_mode {:name "ipython", :version 3}, :file_extension ".py", :mimetype "text/x-python", :name "python", :nbconvert_exporter "python", :pygments_lexer "ipython3", :version "3.10.12"}}, :nbformat 4, :nbformat_minor 5})

(defn extract-cells-from-file [file-name]
  (-> file-name
      slurp
      (json/parse-string true)
      :cells))

(defn build-big-presentation [file-names]
  (let [cells (reduce into (map extract-cells-from-file file-names))]
    (assoc jupyter-template :cells cells)))

(-> *command-line-args* build-big-presentation json/generate-string println)
